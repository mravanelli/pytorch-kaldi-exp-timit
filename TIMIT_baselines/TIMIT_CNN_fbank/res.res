ep=000 tr=['TIMIT_tr'] loss=3.607 err=0.755 valid=TIMIT_dev loss=2.484 err=0.638 lr_architecture1=0.080000 lr_architecture2=0.080000 time(s)=322
ep=001 tr=['TIMIT_tr'] loss=2.380 err=0.620 valid=TIMIT_dev loss=2.214 err=0.591 lr_architecture1=0.080000 lr_architecture2=0.080000 time(s)=320
ep=002 tr=['TIMIT_tr'] loss=2.146 err=0.578 valid=TIMIT_dev loss=2.095 err=0.563 lr_architecture1=0.080000 lr_architecture2=0.080000 time(s)=320
ep=003 tr=['TIMIT_tr'] loss=2.010 err=0.552 valid=TIMIT_dev loss=2.050 err=0.552 lr_architecture1=0.080000 lr_architecture2=0.080000 time(s)=320
ep=004 tr=['TIMIT_tr'] loss=1.909 err=0.532 valid=TIMIT_dev loss=2.026 err=0.546 lr_architecture1=0.080000 lr_architecture2=0.080000 time(s)=323
ep=005 tr=['TIMIT_tr'] loss=1.829 err=0.515 valid=TIMIT_dev loss=1.996 err=0.540 lr_architecture1=0.080000 lr_architecture2=0.080000 time(s)=321
ep=006 tr=['TIMIT_tr'] loss=1.763 err=0.502 valid=TIMIT_dev loss=1.982 err=0.534 lr_architecture1=0.080000 lr_architecture2=0.080000 time(s)=321
ep=007 tr=['TIMIT_tr'] loss=1.707 err=0.490 valid=TIMIT_dev loss=1.981 err=0.533 lr_architecture1=0.080000 lr_architecture2=0.080000 time(s)=322
ep=008 tr=['TIMIT_tr'] loss=1.655 err=0.479 valid=TIMIT_dev loss=1.978 err=0.530 lr_architecture1=0.080000 lr_architecture2=0.080000 time(s)=321
ep=009 tr=['TIMIT_tr'] loss=1.610 err=0.468 valid=TIMIT_dev loss=1.987 err=0.532 lr_architecture1=0.080000 lr_architecture2=0.080000 time(s)=321
ep=010 tr=['TIMIT_tr'] loss=1.509 err=0.444 valid=TIMIT_dev loss=1.920 err=0.516 lr_architecture1=0.040000 lr_architecture2=0.040000 time(s)=323
ep=011 tr=['TIMIT_tr'] loss=1.468 err=0.435 valid=TIMIT_dev loss=1.934 err=0.517 lr_architecture1=0.040000 lr_architecture2=0.040000 time(s)=330
ep=012 tr=['TIMIT_tr'] loss=1.412 err=0.421 valid=TIMIT_dev loss=1.914 err=0.512 lr_architecture1=0.020000 lr_architecture2=0.020000 time(s)=322
ep=013 tr=['TIMIT_tr'] loss=1.386 err=0.415 valid=TIMIT_dev loss=1.927 err=0.512 lr_architecture1=0.020000 lr_architecture2=0.020000 time(s)=324
ep=014 tr=['TIMIT_tr'] loss=1.357 err=0.407 valid=TIMIT_dev loss=1.911 err=0.508 lr_architecture1=0.010000 lr_architecture2=0.010000 time(s)=324
ep=015 tr=['TIMIT_tr'] loss=1.343 err=0.404 valid=TIMIT_dev loss=1.919 err=0.508 lr_architecture1=0.010000 lr_architecture2=0.010000 time(s)=320
ep=016 tr=['TIMIT_tr'] loss=1.331 err=0.401 valid=TIMIT_dev loss=1.921 err=0.509 lr_architecture1=0.010000 lr_architecture2=0.010000 time(s)=321
ep=017 tr=['TIMIT_tr'] loss=1.316 err=0.397 valid=TIMIT_dev loss=1.922 err=0.508 lr_architecture1=0.005000 lr_architecture2=0.005000 time(s)=322
ep=018 tr=['TIMIT_tr'] loss=1.307 err=0.395 valid=TIMIT_dev loss=1.921 err=0.508 lr_architecture1=0.005000 lr_architecture2=0.005000 time(s)=324
ep=019 tr=['TIMIT_tr'] loss=1.302 err=0.394 valid=TIMIT_dev loss=1.915 err=0.507 lr_architecture1=0.002500 lr_architecture2=0.002500 time(s)=325
ep=020 tr=['TIMIT_tr'] loss=1.296 err=0.393 valid=TIMIT_dev loss=1.920 err=0.507 lr_architecture1=0.002500 lr_architecture2=0.002500 time(s)=322
ep=021 tr=['TIMIT_tr'] loss=1.294 err=0.392 valid=TIMIT_dev loss=1.914 err=0.506 lr_architecture1=0.001250 lr_architecture2=0.001250 time(s)=325
ep=022 tr=['TIMIT_tr'] loss=1.292 err=0.391 valid=TIMIT_dev loss=1.920 err=0.506 lr_architecture1=0.001250 lr_architecture2=0.001250 time(s)=323
ep=023 tr=['TIMIT_tr'] loss=1.289 err=0.391 valid=TIMIT_dev loss=1.915 err=0.506 lr_architecture1=0.000625 lr_architecture2=0.000625 time(s)=327
%WER 18.2 | 192 7215 | 84.6 12.0 3.4 2.8 18.2 99.5 | -0.458 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_CNN_fbank_sgd_relu_3cnn_4dnn_run2/decode_TIMIT_test_out_dnn2/score_6/ctm_39phn.filt.sys

