ep=000 tr=['TIMIT_tr'] loss=5.478 err=0.741 valid=TIMIT_dev loss=3.776 err=0.602 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=747
ep=001 tr=['TIMIT_tr'] loss=3.319 err=0.545 valid=TIMIT_dev loss=2.760 err=0.483 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=760
ep=002 tr=['TIMIT_tr'] loss=2.555 err=0.451 valid=TIMIT_dev loss=2.234 err=0.418 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=817
ep=003 tr=['TIMIT_tr'] loss=2.234 err=0.408 valid=TIMIT_dev loss=2.084 err=0.393 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=842
ep=004 tr=['TIMIT_tr'] loss=2.042 err=0.379 valid=TIMIT_dev loss=2.056 err=0.393 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=845
ep=005 tr=['TIMIT_tr'] loss=1.899 err=0.358 valid=TIMIT_dev loss=1.956 err=0.377 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=838
ep=006 tr=['TIMIT_tr'] loss=1.784 err=0.340 valid=TIMIT_dev loss=1.944 err=0.375 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=841
ep=007 tr=['TIMIT_tr'] loss=1.686 err=0.325 valid=TIMIT_dev loss=1.846 err=0.360 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=843
ep=008 tr=['TIMIT_tr'] loss=1.595 err=0.312 valid=TIMIT_dev loss=1.834 err=0.357 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=838
ep=009 tr=['TIMIT_tr'] loss=1.513 err=0.299 valid=TIMIT_dev loss=1.802 err=0.351 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=841
ep=010 tr=['TIMIT_tr'] loss=1.448 err=0.289 valid=TIMIT_dev loss=1.797 err=0.350 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=843
ep=011 tr=['TIMIT_tr'] loss=1.383 err=0.279 valid=TIMIT_dev loss=1.841 err=0.351 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=842
ep=012 tr=['TIMIT_tr'] loss=1.227 err=0.250 valid=TIMIT_dev loss=1.731 err=0.336 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=834
ep=013 tr=['TIMIT_tr'] loss=1.177 err=0.243 valid=TIMIT_dev loss=1.711 err=0.331 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=846
ep=014 tr=['TIMIT_tr'] loss=1.146 err=0.237 valid=TIMIT_dev loss=1.718 err=0.330 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=846
ep=015 tr=['TIMIT_tr'] loss=1.112 err=0.232 valid=TIMIT_dev loss=1.734 err=0.331 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=845
ep=016 tr=['TIMIT_tr'] loss=1.045 err=0.219 valid=TIMIT_dev loss=1.688 err=0.324 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=837
ep=017 tr=['TIMIT_tr'] loss=1.020 err=0.214 valid=TIMIT_dev loss=1.705 err=0.325 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=837
ep=018 tr=['TIMIT_tr'] loss=0.981 err=0.207 valid=TIMIT_dev loss=1.699 err=0.325 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=842
ep=019 tr=['TIMIT_tr'] loss=0.973 err=0.206 valid=TIMIT_dev loss=1.691 err=0.322 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=834
ep=020 tr=['TIMIT_tr'] loss=0.964 err=0.204 valid=TIMIT_dev loss=1.695 err=0.323 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=844
ep=021 tr=['TIMIT_tr'] loss=0.946 err=0.200 valid=TIMIT_dev loss=1.689 err=0.323 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=833
ep=022 tr=['TIMIT_tr'] loss=0.939 err=0.199 valid=TIMIT_dev loss=1.693 err=0.322 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=825
ep=023 tr=['TIMIT_tr'] loss=0.936 err=0.199 valid=TIMIT_dev loss=1.697 err=0.322 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=832
%WER 15.1 | 192 7215 | 87.7 9.7 2.6 2.8 15.1 97.9 | -2.572 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_GRU_fbank_5lay_lr00004_correct_lran_monoreg_v2/decode_TIMIT_test_out_dnn2/score_2/ctm_39phn.filt.sys

