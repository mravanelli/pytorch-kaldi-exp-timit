ep=000 tr=['TIMIT_tr'] loss=5.033 err=0.702 valid=TIMIT_dev loss=3.238 err=0.546 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=730
ep=001 tr=['TIMIT_tr'] loss=2.897 err=0.492 valid=TIMIT_dev loss=2.411 err=0.438 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=750
ep=002 tr=['TIMIT_tr'] loss=2.233 err=0.406 valid=TIMIT_dev loss=2.074 err=0.391 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=829
ep=003 tr=['TIMIT_tr'] loss=1.965 err=0.369 valid=TIMIT_dev loss=1.945 err=0.367 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=834
ep=004 tr=['TIMIT_tr'] loss=1.797 err=0.343 valid=TIMIT_dev loss=1.940 err=0.370 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=830
ep=005 tr=['TIMIT_tr'] loss=1.577 err=0.306 valid=TIMIT_dev loss=1.811 err=0.345 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=837
ep=006 tr=['TIMIT_tr'] loss=1.499 err=0.293 valid=TIMIT_dev loss=1.787 err=0.339 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=839
ep=007 tr=['TIMIT_tr'] loss=1.443 err=0.285 valid=TIMIT_dev loss=1.755 err=0.334 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=836
ep=008 tr=['TIMIT_tr'] loss=1.389 err=0.276 valid=TIMIT_dev loss=1.742 err=0.333 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=830
ep=009 tr=['TIMIT_tr'] loss=1.338 err=0.267 valid=TIMIT_dev loss=1.734 err=0.330 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=833
ep=010 tr=['TIMIT_tr'] loss=1.292 err=0.260 valid=TIMIT_dev loss=1.738 err=0.331 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=838
ep=011 tr=['TIMIT_tr'] loss=1.201 err=0.243 valid=TIMIT_dev loss=1.714 err=0.324 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=868
ep=012 tr=['TIMIT_tr'] loss=1.175 err=0.239 valid=TIMIT_dev loss=1.703 err=0.321 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=853
ep=013 tr=['TIMIT_tr'] loss=1.146 err=0.234 valid=TIMIT_dev loss=1.697 err=0.319 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=844
ep=014 tr=['TIMIT_tr'] loss=1.129 err=0.232 valid=TIMIT_dev loss=1.703 err=0.318 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=842
ep=015 tr=['TIMIT_tr'] loss=1.106 err=0.228 valid=TIMIT_dev loss=1.714 err=0.319 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=836
ep=016 tr=['TIMIT_tr'] loss=1.065 err=0.220 valid=TIMIT_dev loss=1.699 err=0.316 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=882
ep=017 tr=['TIMIT_tr'] loss=1.052 err=0.218 valid=TIMIT_dev loss=1.708 err=0.317 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=845
ep=018 tr=['TIMIT_tr'] loss=1.028 err=0.213 valid=TIMIT_dev loss=1.696 err=0.315 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=852
ep=019 tr=['TIMIT_tr'] loss=1.025 err=0.213 valid=TIMIT_dev loss=1.699 err=0.314 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=852
ep=020 tr=['TIMIT_tr'] loss=1.020 err=0.211 valid=TIMIT_dev loss=1.697 err=0.314 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=845
ep=021 tr=['TIMIT_tr'] loss=1.008 err=0.209 valid=TIMIT_dev loss=1.695 err=0.314 lr_architecture1=0.000013 lr_architecture2=0.000013 lr_architecture3=0.000013 time(s)=838
ep=022 tr=['TIMIT_tr'] loss=1.004 err=0.209 valid=TIMIT_dev loss=1.700 err=0.314 lr_architecture1=0.000013 lr_architecture2=0.000013 lr_architecture3=0.000013 time(s)=835
ep=023 tr=['TIMIT_tr'] loss=0.997 err=0.208 valid=TIMIT_dev loss=1.697 err=0.313 lr_architecture1=0.000006 lr_architecture2=0.000006 lr_architecture3=0.000006 time(s)=839
%WER 14.8 | 192 7215 | 87.9 9.4 2.8 2.6 14.8 99.0 | -1.933 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_GRU_fmllr_5lay_lr00004_correct_lran_monoreg_v2/decode_TIMIT_test_out_dnn2/score_3/ctm_39phn.filt.sys

