ep=000 tr=['TIMIT_tr'] loss=5.349 err=0.731 valid=TIMIT_dev loss=3.563 err=0.583 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=886
ep=001 tr=['TIMIT_tr'] loss=3.257 err=0.538 valid=TIMIT_dev loss=2.666 err=0.474 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=890
ep=002 tr=['TIMIT_tr'] loss=2.520 err=0.446 valid=TIMIT_dev loss=2.227 err=0.416 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=947
ep=003 tr=['TIMIT_tr'] loss=2.214 err=0.404 valid=TIMIT_dev loss=2.112 err=0.395 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=968
ep=004 tr=['TIMIT_tr'] loss=2.027 err=0.377 valid=TIMIT_dev loss=2.109 err=0.398 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=952
ep=005 tr=['TIMIT_tr'] loss=1.776 err=0.335 valid=TIMIT_dev loss=1.918 err=0.367 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=929
ep=006 tr=['TIMIT_tr'] loss=1.691 err=0.321 valid=TIMIT_dev loss=1.895 err=0.361 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=936
ep=007 tr=['TIMIT_tr'] loss=1.627 err=0.312 valid=TIMIT_dev loss=1.859 err=0.356 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=916
ep=008 tr=['TIMIT_tr'] loss=1.564 err=0.303 valid=TIMIT_dev loss=1.829 err=0.352 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=986
ep=009 tr=['TIMIT_tr'] loss=1.505 err=0.293 valid=TIMIT_dev loss=1.832 err=0.350 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=949
ep=010 tr=['TIMIT_tr'] loss=1.452 err=0.285 valid=TIMIT_dev loss=1.815 err=0.348 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=964
ep=011 tr=['TIMIT_tr'] loss=1.406 err=0.278 valid=TIMIT_dev loss=1.814 err=0.348 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=952
ep=012 tr=['TIMIT_tr'] loss=1.314 err=0.262 valid=TIMIT_dev loss=1.782 err=0.342 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=953
ep=013 tr=['TIMIT_tr'] loss=1.275 err=0.256 valid=TIMIT_dev loss=1.777 err=0.339 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=935
ep=014 tr=['TIMIT_tr'] loss=1.253 err=0.251 valid=TIMIT_dev loss=1.775 err=0.338 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=962
ep=015 tr=['TIMIT_tr'] loss=1.231 err=0.248 valid=TIMIT_dev loss=1.776 err=0.339 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=929
ep=016 tr=['TIMIT_tr'] loss=1.186 err=0.240 valid=TIMIT_dev loss=1.762 err=0.335 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=942
ep=017 tr=['TIMIT_tr'] loss=1.169 err=0.238 valid=TIMIT_dev loss=1.772 err=0.336 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=917
ep=018 tr=['TIMIT_tr'] loss=1.147 err=0.234 valid=TIMIT_dev loss=1.757 err=0.334 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=940
ep=019 tr=['TIMIT_tr'] loss=1.140 err=0.232 valid=TIMIT_dev loss=1.755 err=0.332 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=936
ep=020 tr=['TIMIT_tr'] loss=1.135 err=0.231 valid=TIMIT_dev loss=1.762 err=0.334 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=952
ep=021 tr=['TIMIT_tr'] loss=1.124 err=0.229 valid=TIMIT_dev loss=1.758 err=0.333 lr_architecture1=0.000013 lr_architecture2=0.000013 lr_architecture3=0.000013 time(s)=953
ep=022 tr=['TIMIT_tr'] loss=1.122 err=0.229 valid=TIMIT_dev loss=1.759 err=0.333 lr_architecture1=0.000013 lr_architecture2=0.000013 lr_architecture3=0.000013 time(s)=958
ep=023 tr=['TIMIT_tr'] loss=1.112 err=0.226 valid=TIMIT_dev loss=1.757 err=0.333 lr_architecture1=0.000006 lr_architecture2=0.000006 lr_architecture3=0.000006 time(s)=946
%WER 15.8 | 192 7215 | 86.9 10.4 2.7 2.7 15.8 96.4 | -2.527 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_GRU_mfcc_5lay_lr00004_correct_lran_monoreg_v2/decode_TIMIT_test_out_dnn2/score_2/ctm_39phn.filt.sys

