ep=000 tr=['TIMIT_tr'] loss=4.869 err=0.699 valid=TIMIT_dev loss=3.187 err=0.547 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=717
ep=001 tr=['TIMIT_tr'] loss=2.866 err=0.499 valid=TIMIT_dev loss=2.460 err=0.444 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=746
ep=002 tr=['TIMIT_tr'] loss=2.221 err=0.413 valid=TIMIT_dev loss=2.093 err=0.398 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=819
ep=003 tr=['TIMIT_tr'] loss=1.957 err=0.375 valid=TIMIT_dev loss=1.999 err=0.381 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=852
ep=004 tr=['TIMIT_tr'] loss=1.785 err=0.348 valid=TIMIT_dev loss=2.065 err=0.384 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=823
ep=005 tr=['TIMIT_tr'] loss=1.476 err=0.295 valid=TIMIT_dev loss=1.842 err=0.350 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=841
ep=006 tr=['TIMIT_tr'] loss=1.360 err=0.276 valid=TIMIT_dev loss=1.851 err=0.347 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=835
ep=007 tr=['TIMIT_tr'] loss=1.284 err=0.265 valid=TIMIT_dev loss=1.817 err=0.339 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=838
ep=008 tr=['TIMIT_tr'] loss=1.221 err=0.254 valid=TIMIT_dev loss=1.853 err=0.342 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=830
ep=009 tr=['TIMIT_tr'] loss=1.087 err=0.228 valid=TIMIT_dev loss=1.816 err=0.333 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000100 time(s)=845
ep=010 tr=['TIMIT_tr'] loss=1.034 err=0.219 valid=TIMIT_dev loss=1.814 err=0.330 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000100 time(s)=838
ep=011 tr=['TIMIT_tr'] loss=1.000 err=0.213 valid=TIMIT_dev loss=1.838 err=0.331 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000100 time(s)=824
ep=012 tr=['TIMIT_tr'] loss=0.938 err=0.201 valid=TIMIT_dev loss=1.828 err=0.327 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000050 time(s)=830
ep=013 tr=['TIMIT_tr'] loss=0.915 err=0.198 valid=TIMIT_dev loss=1.823 err=0.327 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000050 time(s)=827
ep=014 tr=['TIMIT_tr'] loss=0.894 err=0.193 valid=TIMIT_dev loss=1.830 err=0.326 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000050 time(s)=837
ep=015 tr=['TIMIT_tr'] loss=0.865 err=0.187 valid=TIMIT_dev loss=1.824 err=0.325 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000025 time(s)=824
ep=016 tr=['TIMIT_tr'] loss=0.851 err=0.185 valid=TIMIT_dev loss=1.820 err=0.324 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000025 time(s)=859
ep=017 tr=['TIMIT_tr'] loss=0.839 err=0.183 valid=TIMIT_dev loss=1.830 err=0.324 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000025 time(s)=836
ep=018 tr=['TIMIT_tr'] loss=0.827 err=0.180 valid=TIMIT_dev loss=1.833 err=0.324 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000013 time(s)=831
ep=019 tr=['TIMIT_tr'] loss=0.825 err=0.179 valid=TIMIT_dev loss=1.829 err=0.323 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000013 time(s)=814
ep=020 tr=['TIMIT_tr'] loss=0.815 err=0.178 valid=TIMIT_dev loss=1.839 err=0.323 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000013 time(s)=821
ep=021 tr=['TIMIT_tr'] loss=0.811 err=0.177 valid=TIMIT_dev loss=1.834 err=0.323 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000006 time(s)=841
ep=022 tr=['TIMIT_tr'] loss=0.804 err=0.175 valid=TIMIT_dev loss=1.836 err=0.322 lr_architecture1=0.000013 lr_architecture2=0.000013 lr_architecture3=0.000003 time(s)=833
ep=023 tr=['TIMIT_tr'] loss=0.804 err=0.175 valid=TIMIT_dev loss=1.834 err=0.323 lr_architecture1=0.000013 lr_architecture2=0.000013 lr_architecture3=0.000003 time(s)=847
%WER 14.2 | 192 7215 | 88.0 9.1 2.9 2.3 14.2 97.9 | -2.422 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_LSTM_fbank_lr00016_lran_monoreg_v2/decode_TIMIT_test_out_dnn2/score_3/ctm_39phn.filt.sys

