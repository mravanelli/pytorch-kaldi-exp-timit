ep=000 tr=['TIMIT_tr'] loss=4.423 err=0.657 valid=TIMIT_dev loss=2.758 err=0.489 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=760
ep=001 tr=['TIMIT_tr'] loss=2.533 err=0.455 valid=TIMIT_dev loss=2.225 err=0.410 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=751
ep=002 tr=['TIMIT_tr'] loss=1.957 err=0.373 valid=TIMIT_dev loss=1.994 err=0.367 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=836
ep=003 tr=['TIMIT_tr'] loss=1.726 err=0.338 valid=TIMIT_dev loss=1.948 err=0.355 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=864
ep=004 tr=['TIMIT_tr'] loss=1.562 err=0.313 valid=TIMIT_dev loss=2.012 err=0.360 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=854
ep=005 tr=['TIMIT_tr'] loss=1.288 err=0.264 valid=TIMIT_dev loss=1.893 err=0.340 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=851
ep=006 tr=['TIMIT_tr'] loss=1.180 err=0.245 valid=TIMIT_dev loss=1.862 err=0.331 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=836
ep=007 tr=['TIMIT_tr'] loss=1.110 err=0.234 valid=TIMIT_dev loss=1.900 err=0.330 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=834
ep=008 tr=['TIMIT_tr'] loss=1.049 err=0.223 valid=TIMIT_dev loss=1.898 err=0.327 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=823
ep=009 tr=['TIMIT_tr'] loss=0.994 err=0.214 valid=TIMIT_dev loss=1.910 err=0.325 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=856
ep=010 tr=['TIMIT_tr'] loss=0.943 err=0.205 valid=TIMIT_dev loss=1.923 err=0.322 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=839
ep=011 tr=['TIMIT_tr'] loss=0.894 err=0.196 valid=TIMIT_dev loss=1.983 err=0.326 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=862
ep=012 tr=['TIMIT_tr'] loss=0.793 err=0.175 valid=TIMIT_dev loss=1.963 err=0.319 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000100 time(s)=836
ep=013 tr=['TIMIT_tr'] loss=0.751 err=0.167 valid=TIMIT_dev loss=1.989 err=0.318 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000100 time(s)=845
ep=014 tr=['TIMIT_tr'] loss=0.724 err=0.163 valid=TIMIT_dev loss=2.023 err=0.319 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000100 time(s)=842
ep=015 tr=['TIMIT_tr'] loss=0.679 err=0.153 valid=TIMIT_dev loss=2.028 err=0.316 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000050 time(s)=843
ep=016 tr=['TIMIT_tr'] loss=0.654 err=0.148 valid=TIMIT_dev loss=2.056 err=0.317 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000050 time(s)=881
ep=017 tr=['TIMIT_tr'] loss=0.631 err=0.144 valid=TIMIT_dev loss=2.061 err=0.315 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000025 time(s)=834
ep=018 tr=['TIMIT_tr'] loss=0.621 err=0.142 valid=TIMIT_dev loss=2.071 err=0.316 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000025 time(s)=842
ep=019 tr=['TIMIT_tr'] loss=0.607 err=0.138 valid=TIMIT_dev loss=2.072 err=0.316 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000013 time(s)=851
ep=020 tr=['TIMIT_tr'] loss=0.605 err=0.138 valid=TIMIT_dev loss=2.081 err=0.316 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000013 time(s)=838
ep=021 tr=['TIMIT_tr'] loss=0.597 err=0.136 valid=TIMIT_dev loss=2.081 err=0.315 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000006 time(s)=857
ep=022 tr=['TIMIT_tr'] loss=0.593 err=0.135 valid=TIMIT_dev loss=2.086 err=0.315 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000006 time(s)=840
ep=023 tr=['TIMIT_tr'] loss=0.593 err=0.136 valid=TIMIT_dev loss=2.084 err=0.315 lr_architecture1=0.000013 lr_architecture2=0.000013 lr_architecture3=0.000003 time(s)=861
%WER 14.5 | 192 7215 | 87.5 9.2 3.4 2.0 14.5 98.4 | -1.412 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_LSTM_fmllr_lr00016_lran_monoreg_v2/decode_TIMIT_test_out_dnn2/score_9/ctm_39phn.filt.sys

