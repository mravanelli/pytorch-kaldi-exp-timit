ep=000 tr=['TIMIT_tr'] loss=4.858 err=0.695 valid=TIMIT_dev loss=3.061 err=0.532 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=734
ep=001 tr=['TIMIT_tr'] loss=2.785 err=0.490 valid=TIMIT_dev loss=2.413 err=0.440 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=741
ep=002 tr=['TIMIT_tr'] loss=2.186 err=0.406 valid=TIMIT_dev loss=2.084 err=0.391 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=826
ep=003 tr=['TIMIT_tr'] loss=1.927 err=0.368 valid=TIMIT_dev loss=2.032 err=0.377 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=827
ep=004 tr=['TIMIT_tr'] loss=1.748 err=0.342 valid=TIMIT_dev loss=2.072 err=0.381 lr_architecture1=0.001600 lr_architecture2=0.001600 lr_architecture3=0.000400 time(s)=845
ep=005 tr=['TIMIT_tr'] loss=1.439 err=0.288 valid=TIMIT_dev loss=1.931 err=0.356 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=833
ep=006 tr=['TIMIT_tr'] loss=1.325 err=0.270 valid=TIMIT_dev loss=1.929 err=0.351 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=829
ep=007 tr=['TIMIT_tr'] loss=1.246 err=0.257 valid=TIMIT_dev loss=1.897 err=0.344 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=832
ep=008 tr=['TIMIT_tr'] loss=1.174 err=0.245 valid=TIMIT_dev loss=1.932 err=0.347 lr_architecture1=0.000800 lr_architecture2=0.000800 lr_architecture3=0.000200 time(s)=836
ep=009 tr=['TIMIT_tr'] loss=1.042 err=0.220 valid=TIMIT_dev loss=1.898 err=0.335 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000100 time(s)=842
ep=010 tr=['TIMIT_tr'] loss=0.994 err=0.212 valid=TIMIT_dev loss=1.905 err=0.335 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000100 time(s)=822
ep=011 tr=['TIMIT_tr'] loss=0.923 err=0.198 valid=TIMIT_dev loss=1.910 err=0.331 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000050 time(s)=834
ep=012 tr=['TIMIT_tr'] loss=0.896 err=0.193 valid=TIMIT_dev loss=1.927 err=0.332 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000050 time(s)=833
ep=013 tr=['TIMIT_tr'] loss=0.866 err=0.187 valid=TIMIT_dev loss=1.919 err=0.329 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000025 time(s)=839
ep=014 tr=['TIMIT_tr'] loss=0.854 err=0.185 valid=TIMIT_dev loss=1.931 err=0.329 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000025 time(s)=838
ep=015 tr=['TIMIT_tr'] loss=0.834 err=0.181 valid=TIMIT_dev loss=1.931 err=0.328 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000013 time(s)=835
ep=016 tr=['TIMIT_tr'] loss=0.830 err=0.180 valid=TIMIT_dev loss=1.931 err=0.328 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000013 time(s)=845
ep=017 tr=['TIMIT_tr'] loss=0.822 err=0.179 valid=TIMIT_dev loss=1.937 err=0.327 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000013 time(s)=837
ep=018 tr=['TIMIT_tr'] loss=0.818 err=0.178 valid=TIMIT_dev loss=1.943 err=0.327 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000013 time(s)=836
ep=019 tr=['TIMIT_tr'] loss=0.812 err=0.177 valid=TIMIT_dev loss=1.938 err=0.327 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000006 time(s)=822
ep=020 tr=['TIMIT_tr'] loss=0.806 err=0.176 valid=TIMIT_dev loss=1.943 err=0.327 lr_architecture1=0.000013 lr_architecture2=0.000013 lr_architecture3=0.000003 time(s)=814
ep=021 tr=['TIMIT_tr'] loss=0.809 err=0.176 valid=TIMIT_dev loss=1.941 err=0.327 lr_architecture1=0.000006 lr_architecture2=0.000006 lr_architecture3=0.000002 time(s)=822
ep=022 tr=['TIMIT_tr'] loss=0.801 err=0.174 valid=TIMIT_dev loss=1.943 err=0.327 lr_architecture1=0.000003 lr_architecture2=0.000003 lr_architecture3=0.000001 time(s)=822
ep=023 tr=['TIMIT_tr'] loss=0.803 err=0.175 valid=TIMIT_dev loss=1.942 err=0.327 lr_architecture1=0.000002 lr_architecture2=0.000002 lr_architecture3=0.000000 time(s)=820
%WER 15.0 | 192 7215 | 87.3 9.7 3.0 2.3 15.0 97.4 | -2.422 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_LSTM_mfcc_lr00016_lran_monoreg_v2/decode_TIMIT_test_out_dnn2/score_3/ctm_39phn.filt.sys

