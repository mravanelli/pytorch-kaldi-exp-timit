ep=000 tr=['TIMIT_tr'] loss=3.398 err=0.721 valid=TIMIT_dev loss=2.268 err=0.591 lr_architecture1=0.080000 time(s)=82
ep=001 tr=['TIMIT_tr'] loss=2.137 err=0.570 valid=TIMIT_dev loss=1.990 err=0.541 lr_architecture1=0.080000 time(s)=81
ep=002 tr=['TIMIT_tr'] loss=1.896 err=0.524 valid=TIMIT_dev loss=1.874 err=0.516 lr_architecture1=0.080000 time(s)=83
ep=003 tr=['TIMIT_tr'] loss=1.751 err=0.494 valid=TIMIT_dev loss=1.819 err=0.504 lr_architecture1=0.080000 time(s)=86
ep=004 tr=['TIMIT_tr'] loss=1.645 err=0.472 valid=TIMIT_dev loss=1.775 err=0.494 lr_architecture1=0.080000 time(s)=83
ep=005 tr=['TIMIT_tr'] loss=1.560 err=0.453 valid=TIMIT_dev loss=1.773 err=0.493 lr_architecture1=0.080000 time(s)=83
ep=006 tr=['TIMIT_tr'] loss=1.489 err=0.436 valid=TIMIT_dev loss=1.744 err=0.484 lr_architecture1=0.080000 time(s)=82
ep=007 tr=['TIMIT_tr'] loss=1.430 err=0.423 valid=TIMIT_dev loss=1.745 err=0.483 lr_architecture1=0.080000 time(s)=81
ep=008 tr=['TIMIT_tr'] loss=1.376 err=0.410 valid=TIMIT_dev loss=1.729 err=0.479 lr_architecture1=0.080000 time(s)=81
ep=009 tr=['TIMIT_tr'] loss=1.329 err=0.399 valid=TIMIT_dev loss=1.726 err=0.474 lr_architecture1=0.080000 time(s)=83
ep=010 tr=['TIMIT_tr'] loss=1.287 err=0.389 valid=TIMIT_dev loss=1.725 err=0.474 lr_architecture1=0.080000 time(s)=80
ep=011 tr=['TIMIT_tr'] loss=1.183 err=0.362 valid=TIMIT_dev loss=1.663 err=0.458 lr_architecture1=0.040000 time(s)=81
ep=012 tr=['TIMIT_tr'] loss=1.141 err=0.352 valid=TIMIT_dev loss=1.678 err=0.460 lr_architecture1=0.040000 time(s)=81
ep=013 tr=['TIMIT_tr'] loss=1.085 err=0.336 valid=TIMIT_dev loss=1.646 err=0.452 lr_architecture1=0.020000 time(s)=81
ep=014 tr=['TIMIT_tr'] loss=1.060 err=0.329 valid=TIMIT_dev loss=1.662 err=0.450 lr_architecture1=0.020000 time(s)=81
ep=015 tr=['TIMIT_tr'] loss=1.044 err=0.325 valid=TIMIT_dev loss=1.661 err=0.451 lr_architecture1=0.020000 time(s)=81
ep=016 tr=['TIMIT_tr'] loss=1.015 err=0.317 valid=TIMIT_dev loss=1.653 err=0.448 lr_architecture1=0.010000 time(s)=81
ep=017 tr=['TIMIT_tr'] loss=1.002 err=0.314 valid=TIMIT_dev loss=1.655 err=0.447 lr_architecture1=0.010000 time(s)=81
ep=018 tr=['TIMIT_tr'] loss=0.993 err=0.311 valid=TIMIT_dev loss=1.660 err=0.449 lr_architecture1=0.010000 time(s)=81
ep=019 tr=['TIMIT_tr'] loss=0.978 err=0.307 valid=TIMIT_dev loss=1.655 err=0.449 lr_architecture1=0.005000 time(s)=81
ep=020 tr=['TIMIT_tr'] loss=0.968 err=0.304 valid=TIMIT_dev loss=1.648 err=0.446 lr_architecture1=0.002500 time(s)=81
ep=021 tr=['TIMIT_tr'] loss=0.965 err=0.304 valid=TIMIT_dev loss=1.649 err=0.446 lr_architecture1=0.002500 time(s)=81
ep=022 tr=['TIMIT_tr'] loss=0.960 err=0.302 valid=TIMIT_dev loss=1.652 err=0.447 lr_architecture1=0.001250 time(s)=81
ep=023 tr=['TIMIT_tr'] loss=0.959 err=0.301 valid=TIMIT_dev loss=1.651 err=0.446 lr_architecture1=0.000625 time(s)=81
%WER 18.1 | 192 7215 | 84.0 11.9 4.2 2.1 18.1 99.5 | -0.583 | /home/mirco/pytorch-kaldi-new/exp/TIMIT_MLP_basic/decode_TIMIT_test_out_dnn1/score_6/ctm_39phn.filt.sys

