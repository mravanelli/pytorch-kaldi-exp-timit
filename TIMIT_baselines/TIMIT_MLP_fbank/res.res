ep=000 tr=['TIMIT_tr'] loss=4.796 err=0.748 valid=TIMIT_dev loss=3.837 err=0.663 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=186
ep=001 tr=['TIMIT_tr'] loss=3.744 err=0.652 valid=TIMIT_dev loss=3.454 err=0.617 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=176
ep=002 tr=['TIMIT_tr'] loss=3.445 err=0.618 valid=TIMIT_dev loss=3.261 err=0.593 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=176
ep=003 tr=['TIMIT_tr'] loss=3.274 err=0.597 valid=TIMIT_dev loss=3.159 err=0.579 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=185
ep=004 tr=['TIMIT_tr'] loss=3.158 err=0.583 valid=TIMIT_dev loss=3.087 err=0.569 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=189
ep=005 tr=['TIMIT_tr'] loss=3.073 err=0.572 valid=TIMIT_dev loss=3.063 err=0.569 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=185
ep=006 tr=['TIMIT_tr'] loss=2.898 err=0.550 valid=TIMIT_dev loss=2.921 err=0.547 lr_architecture1=0.040000 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=181
ep=007 tr=['TIMIT_tr'] loss=2.832 err=0.542 valid=TIMIT_dev loss=2.902 err=0.543 lr_architecture1=0.040000 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=176
ep=008 tr=['TIMIT_tr'] loss=2.790 err=0.537 valid=TIMIT_dev loss=2.888 err=0.541 lr_architecture1=0.040000 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=176
ep=009 tr=['TIMIT_tr'] loss=2.751 err=0.532 valid=TIMIT_dev loss=2.882 err=0.541 lr_architecture1=0.040000 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=189
ep=010 tr=['TIMIT_tr'] loss=2.647 err=0.519 valid=TIMIT_dev loss=2.833 err=0.532 lr_architecture1=0.020000 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=175
ep=011 tr=['TIMIT_tr'] loss=2.607 err=0.514 valid=TIMIT_dev loss=2.840 err=0.531 lr_architecture1=0.020000 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=182
ep=012 tr=['TIMIT_tr'] loss=2.578 err=0.510 valid=TIMIT_dev loss=2.828 err=0.529 lr_architecture1=0.020000 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=187
ep=013 tr=['TIMIT_tr'] loss=2.557 err=0.507 valid=TIMIT_dev loss=2.827 err=0.529 lr_architecture1=0.020000 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=171
ep=014 tr=['TIMIT_tr'] loss=2.493 err=0.499 valid=TIMIT_dev loss=2.795 err=0.524 lr_architecture1=0.010000 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=183
ep=015 tr=['TIMIT_tr'] loss=2.468 err=0.496 valid=TIMIT_dev loss=2.796 err=0.525 lr_architecture1=0.010000 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=175
ep=016 tr=['TIMIT_tr'] loss=2.435 err=0.492 valid=TIMIT_dev loss=2.792 err=0.521 lr_architecture1=0.005000 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=184
ep=017 tr=['TIMIT_tr'] loss=2.417 err=0.490 valid=TIMIT_dev loss=2.800 err=0.522 lr_architecture1=0.005000 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=194
ep=018 tr=['TIMIT_tr'] loss=2.401 err=0.487 valid=TIMIT_dev loss=2.784 err=0.521 lr_architecture1=0.002500 lr_architecture2=0.000013 lr_architecture3=0.000013 time(s)=196
ep=019 tr=['TIMIT_tr'] loss=2.393 err=0.486 valid=TIMIT_dev loss=2.783 err=0.520 lr_architecture1=0.002500 lr_architecture2=0.000013 lr_architecture3=0.000013 time(s)=191
ep=020 tr=['TIMIT_tr'] loss=2.384 err=0.485 valid=TIMIT_dev loss=2.784 err=0.522 lr_architecture1=0.002500 lr_architecture2=0.000013 lr_architecture3=0.000013 time(s)=170
ep=021 tr=['TIMIT_tr'] loss=2.377 err=0.484 valid=TIMIT_dev loss=2.780 err=0.520 lr_architecture1=0.001250 lr_architecture2=0.000006 lr_architecture3=0.000006 time(s)=171
ep=022 tr=['TIMIT_tr'] loss=2.371 err=0.483 valid=TIMIT_dev loss=2.785 err=0.520 lr_architecture1=0.001250 lr_architecture2=0.000006 lr_architecture3=0.000006 time(s)=188
ep=023 tr=['TIMIT_tr'] loss=2.367 err=0.482 valid=TIMIT_dev loss=2.775 err=0.520 lr_architecture1=0.000625 lr_architecture2=0.000003 lr_architecture3=0.000003 time(s)=184
%WER 18.8 | 192 7215 | 83.9 12.4 3.7 2.7 18.8 99.5 | -0.454 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_MLP_fbank_5lay_lran_monoreg_run2/decode_TIMIT_test_out_dnn2/score_5/ctm_39phn.filt.sys

