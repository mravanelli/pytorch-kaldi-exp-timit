ep=000 tr=['TIMIT_tr'] loss=3.854 err=0.655 valid=TIMIT_dev loss=2.993 err=0.556 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=199
ep=001 tr=['TIMIT_tr'] loss=2.850 err=0.538 valid=TIMIT_dev loss=2.714 err=0.515 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=164
ep=002 tr=['TIMIT_tr'] loss=2.594 err=0.501 valid=TIMIT_dev loss=2.566 err=0.492 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=159
ep=003 tr=['TIMIT_tr'] loss=2.442 err=0.480 valid=TIMIT_dev loss=2.492 err=0.479 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=154
ep=004 tr=['TIMIT_tr'] loss=2.335 err=0.465 valid=TIMIT_dev loss=2.451 err=0.470 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=164
ep=005 tr=['TIMIT_tr'] loss=2.259 err=0.453 valid=TIMIT_dev loss=2.425 err=0.464 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=157
ep=006 tr=['TIMIT_tr'] loss=2.192 err=0.444 valid=TIMIT_dev loss=2.399 err=0.457 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=160
ep=007 tr=['TIMIT_tr'] loss=2.137 err=0.436 valid=TIMIT_dev loss=2.405 err=0.457 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=154
ep=008 tr=['TIMIT_tr'] loss=2.005 err=0.417 valid=TIMIT_dev loss=2.331 err=0.444 lr_architecture1=0.040000 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=156
ep=009 tr=['TIMIT_tr'] loss=1.947 err=0.408 valid=TIMIT_dev loss=2.315 err=0.441 lr_architecture1=0.040000 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=163
ep=010 tr=['TIMIT_tr'] loss=1.914 err=0.404 valid=TIMIT_dev loss=2.340 err=0.443 lr_architecture1=0.040000 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=166
ep=011 tr=['TIMIT_tr'] loss=1.837 err=0.392 valid=TIMIT_dev loss=2.308 err=0.436 lr_architecture1=0.020000 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=161
ep=012 tr=['TIMIT_tr'] loss=1.802 err=0.387 valid=TIMIT_dev loss=2.297 err=0.435 lr_architecture1=0.020000 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=165
ep=013 tr=['TIMIT_tr'] loss=1.782 err=0.383 valid=TIMIT_dev loss=2.305 err=0.433 lr_architecture1=0.020000 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=159
ep=014 tr=['TIMIT_tr'] loss=1.762 err=0.381 valid=TIMIT_dev loss=2.308 err=0.433 lr_architecture1=0.020000 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=164
ep=015 tr=['TIMIT_tr'] loss=1.720 err=0.374 valid=TIMIT_dev loss=2.302 err=0.431 lr_architecture1=0.010000 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=161
ep=016 tr=['TIMIT_tr'] loss=1.699 err=0.371 valid=TIMIT_dev loss=2.310 err=0.431 lr_architecture1=0.010000 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=159
ep=017 tr=['TIMIT_tr'] loss=1.674 err=0.367 valid=TIMIT_dev loss=2.311 err=0.430 lr_architecture1=0.005000 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=163
ep=018 tr=['TIMIT_tr'] loss=1.666 err=0.366 valid=TIMIT_dev loss=2.304 err=0.431 lr_architecture1=0.005000 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=163
ep=019 tr=['TIMIT_tr'] loss=1.649 err=0.363 valid=TIMIT_dev loss=2.299 err=0.429 lr_architecture1=0.002500 lr_architecture2=0.000013 lr_architecture3=0.000013 time(s)=164
ep=020 tr=['TIMIT_tr'] loss=1.643 err=0.362 valid=TIMIT_dev loss=2.304 err=0.429 lr_architecture1=0.002500 lr_architecture2=0.000013 lr_architecture3=0.000013 time(s)=191
ep=021 tr=['TIMIT_tr'] loss=1.639 err=0.362 valid=TIMIT_dev loss=2.300 err=0.429 lr_architecture1=0.002500 lr_architecture2=0.000013 lr_architecture3=0.000013 time(s)=174
ep=022 tr=['TIMIT_tr'] loss=1.634 err=0.362 valid=TIMIT_dev loss=2.299 err=0.428 lr_architecture1=0.001250 lr_architecture2=0.000006 lr_architecture3=0.000006 time(s)=161
ep=023 tr=['TIMIT_tr'] loss=1.629 err=0.360 valid=TIMIT_dev loss=2.304 err=0.428 lr_architecture1=0.001250 lr_architecture2=0.000006 lr_architecture3=0.000006 time(s)=159
%WER 16.6 | 192 7215 | 85.7 10.7 3.5 2.3 16.6 99.5 | -0.844 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_MLP_fmllr_5lay_lran_monoreg_run2/decode_TIMIT_test_out_dnn2/score_5/ctm_39phn.filt.sys

