ep=000 tr=['TIMIT_tr'] loss=4.364 err=0.707 valid=TIMIT_dev loss=3.380 err=0.608 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=168
ep=001 tr=['TIMIT_tr'] loss=3.273 err=0.592 valid=TIMIT_dev loss=3.001 err=0.557 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=182
ep=002 tr=['TIMIT_tr'] loss=2.981 err=0.555 valid=TIMIT_dev loss=2.855 err=0.538 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=164
ep=003 tr=['TIMIT_tr'] loss=2.815 err=0.532 valid=TIMIT_dev loss=2.749 err=0.519 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=176
ep=004 tr=['TIMIT_tr'] loss=2.696 err=0.517 valid=TIMIT_dev loss=2.702 err=0.512 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=173
ep=005 tr=['TIMIT_tr'] loss=2.605 err=0.504 valid=TIMIT_dev loss=2.663 err=0.507 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=184
ep=006 tr=['TIMIT_tr'] loss=2.530 err=0.495 valid=TIMIT_dev loss=2.643 err=0.502 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=183
ep=007 tr=['TIMIT_tr'] loss=2.472 err=0.487 valid=TIMIT_dev loss=2.627 err=0.496 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=176
ep=008 tr=['TIMIT_tr'] loss=2.422 err=0.480 valid=TIMIT_dev loss=2.621 err=0.493 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=176
ep=009 tr=['TIMIT_tr'] loss=2.384 err=0.474 valid=TIMIT_dev loss=2.621 err=0.495 lr_architecture1=0.080000 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=181
ep=010 tr=['TIMIT_tr'] loss=2.236 err=0.454 valid=TIMIT_dev loss=2.518 err=0.477 lr_architecture1=0.040000 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=174
ep=011 tr=['TIMIT_tr'] loss=2.175 err=0.446 valid=TIMIT_dev loss=2.538 err=0.476 lr_architecture1=0.040000 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=184
ep=012 tr=['TIMIT_tr'] loss=2.144 err=0.442 valid=TIMIT_dev loss=2.541 err=0.476 lr_architecture1=0.040000 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=171
ep=013 tr=['TIMIT_tr'] loss=2.062 err=0.430 valid=TIMIT_dev loss=2.499 err=0.469 lr_architecture1=0.020000 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=167
ep=014 tr=['TIMIT_tr'] loss=2.021 err=0.424 valid=TIMIT_dev loss=2.517 err=0.468 lr_architecture1=0.020000 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=175
ep=015 tr=['TIMIT_tr'] loss=2.001 err=0.421 valid=TIMIT_dev loss=2.506 err=0.469 lr_architecture1=0.020000 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=163
ep=016 tr=['TIMIT_tr'] loss=1.953 err=0.415 valid=TIMIT_dev loss=2.509 err=0.466 lr_architecture1=0.010000 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=178
ep=017 tr=['TIMIT_tr'] loss=1.934 err=0.411 valid=TIMIT_dev loss=2.498 err=0.466 lr_architecture1=0.010000 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=181
ep=018 tr=['TIMIT_tr'] loss=1.918 err=0.409 valid=TIMIT_dev loss=2.505 err=0.466 lr_architecture1=0.010000 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=169
ep=019 tr=['TIMIT_tr'] loss=1.896 err=0.406 valid=TIMIT_dev loss=2.506 err=0.464 lr_architecture1=0.005000 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=162
ep=020 tr=['TIMIT_tr'] loss=1.886 err=0.405 valid=TIMIT_dev loss=2.499 err=0.464 lr_architecture1=0.005000 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=163
ep=021 tr=['TIMIT_tr'] loss=1.872 err=0.403 valid=TIMIT_dev loss=2.506 err=0.464 lr_architecture1=0.002500 lr_architecture2=0.000013 lr_architecture3=0.000013 time(s)=160
ep=022 tr=['TIMIT_tr'] loss=1.863 err=0.402 valid=TIMIT_dev loss=2.504 err=0.463 lr_architecture1=0.001250 lr_architecture2=0.000006 lr_architecture3=0.000006 time(s)=168
ep=023 tr=['TIMIT_tr'] loss=1.861 err=0.401 valid=TIMIT_dev loss=2.507 err=0.463 lr_architecture1=0.001250 lr_architecture2=0.000006 lr_architecture3=0.000006 time(s)=168
%WER 18.3 | 192 7215 | 84.1 12.1 3.8 2.4 18.3 99.5 | -1.059 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_MLP_mfcc_5lay_lran_monoreg_run3/decode_TIMIT_test_out_dnn2/score_4/ctm_39phn.filt.sys

