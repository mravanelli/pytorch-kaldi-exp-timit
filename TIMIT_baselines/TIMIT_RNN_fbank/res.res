ep=000 tr=['TIMIT_tr'] loss=5.540 err=0.767 valid=TIMIT_dev loss=3.710 err=0.615 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=311
ep=001 tr=['TIMIT_tr'] loss=3.736 err=0.618 valid=TIMIT_dev loss=3.028 err=0.534 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=286
ep=002 tr=['TIMIT_tr'] loss=3.197 err=0.555 valid=TIMIT_dev loss=2.702 err=0.489 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=302
ep=003 tr=['TIMIT_tr'] loss=2.955 err=0.524 valid=TIMIT_dev loss=2.546 err=0.466 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=300
ep=004 tr=['TIMIT_tr'] loss=2.769 err=0.499 valid=TIMIT_dev loss=2.444 err=0.453 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=302
ep=005 tr=['TIMIT_tr'] loss=2.638 err=0.480 valid=TIMIT_dev loss=2.341 err=0.438 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=302
ep=006 tr=['TIMIT_tr'] loss=2.528 err=0.464 valid=TIMIT_dev loss=2.281 err=0.428 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=307
ep=007 tr=['TIMIT_tr'] loss=2.430 err=0.450 valid=TIMIT_dev loss=2.221 err=0.422 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=298
ep=008 tr=['TIMIT_tr'] loss=2.348 err=0.438 valid=TIMIT_dev loss=2.198 err=0.417 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=308
ep=009 tr=['TIMIT_tr'] loss=2.280 err=0.428 valid=TIMIT_dev loss=2.169 err=0.410 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=309
ep=010 tr=['TIMIT_tr'] loss=2.217 err=0.418 valid=TIMIT_dev loss=2.118 err=0.405 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=303
ep=011 tr=['TIMIT_tr'] loss=2.164 err=0.410 valid=TIMIT_dev loss=2.111 err=0.402 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=305
ep=012 tr=['TIMIT_tr'] loss=2.106 err=0.400 valid=TIMIT_dev loss=2.086 err=0.399 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=318
ep=013 tr=['TIMIT_tr'] loss=2.062 err=0.394 valid=TIMIT_dev loss=2.044 err=0.392 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=301
ep=014 tr=['TIMIT_tr'] loss=2.025 err=0.388 valid=TIMIT_dev loss=2.009 err=0.385 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=307
ep=015 tr=['TIMIT_tr'] loss=1.983 err=0.382 valid=TIMIT_dev loss=2.016 err=0.386 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=304
ep=016 tr=['TIMIT_tr'] loss=1.859 err=0.363 valid=TIMIT_dev loss=1.947 err=0.377 lr_architecture1=0.000160 lr_architecture2=0.000160 lr_architecture3=0.000200 time(s)=305
ep=017 tr=['TIMIT_tr'] loss=1.822 err=0.357 valid=TIMIT_dev loss=1.947 err=0.377 lr_architecture1=0.000160 lr_architecture2=0.000160 lr_architecture3=0.000200 time(s)=309
ep=018 tr=['TIMIT_tr'] loss=1.767 err=0.348 valid=TIMIT_dev loss=1.929 err=0.373 lr_architecture1=0.000080 lr_architecture2=0.000080 lr_architecture3=0.000100 time(s)=307
ep=019 tr=['TIMIT_tr'] loss=1.752 err=0.345 valid=TIMIT_dev loss=1.910 err=0.370 lr_architecture1=0.000080 lr_architecture2=0.000080 lr_architecture3=0.000100 time(s)=308
ep=020 tr=['TIMIT_tr'] loss=1.724 err=0.342 valid=TIMIT_dev loss=1.903 err=0.369 lr_architecture1=0.000080 lr_architecture2=0.000080 lr_architecture3=0.000100 time(s)=306
ep=021 tr=['TIMIT_tr'] loss=1.714 err=0.340 valid=TIMIT_dev loss=1.907 err=0.370 lr_architecture1=0.000080 lr_architecture2=0.000080 lr_architecture3=0.000100 time(s)=301
ep=022 tr=['TIMIT_tr'] loss=1.682 err=0.334 valid=TIMIT_dev loss=1.898 err=0.368 lr_architecture1=0.000040 lr_architecture2=0.000040 lr_architecture3=0.000050 time(s)=315
ep=023 tr=['TIMIT_tr'] loss=1.674 err=0.334 valid=TIMIT_dev loss=1.898 err=0.367 lr_architecture1=0.000040 lr_architecture2=0.000040 lr_architecture3=0.000050 time(s)=301
%WER 17.2 | 192 7215 | 85.8 11.2 3.0 3.0 17.2 99.0 | -1.669 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_RNN_fbank_lr000032_lran_monoref_v2/decode_TIMIT_test_out_dnn2/score_3/ctm_39phn.filt.sys

