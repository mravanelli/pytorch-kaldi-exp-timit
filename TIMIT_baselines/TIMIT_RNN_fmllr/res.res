ep=000 tr=['TIMIT_tr'] loss=6.424 err=0.741 valid=TIMIT_dev loss=3.185 err=0.566 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=328
ep=001 tr=['TIMIT_tr'] loss=3.199 err=0.562 valid=TIMIT_dev loss=2.619 err=0.481 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=287
ep=002 tr=['TIMIT_tr'] loss=2.710 err=0.496 valid=TIMIT_dev loss=2.377 err=0.442 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=303
ep=003 tr=['TIMIT_tr'] loss=2.490 err=0.464 valid=TIMIT_dev loss=2.224 err=0.418 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=306
ep=004 tr=['TIMIT_tr'] loss=2.337 err=0.442 valid=TIMIT_dev loss=2.204 err=0.412 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=308
ep=005 tr=['TIMIT_tr'] loss=2.215 err=0.423 valid=TIMIT_dev loss=2.137 err=0.402 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=303
ep=006 tr=['TIMIT_tr'] loss=2.113 err=0.406 valid=TIMIT_dev loss=2.096 err=0.394 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=313
ep=007 tr=['TIMIT_tr'] loss=2.034 err=0.395 valid=TIMIT_dev loss=2.032 err=0.382 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=310
ep=008 tr=['TIMIT_tr'] loss=1.967 err=0.384 valid=TIMIT_dev loss=1.999 err=0.377 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=309
ep=009 tr=['TIMIT_tr'] loss=1.904 err=0.373 valid=TIMIT_dev loss=1.992 err=0.374 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=311
ep=010 tr=['TIMIT_tr'] loss=1.840 err=0.362 valid=TIMIT_dev loss=1.955 err=0.368 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=305
ep=011 tr=['TIMIT_tr'] loss=1.793 err=0.355 valid=TIMIT_dev loss=1.947 err=0.367 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=327
ep=012 tr=['TIMIT_tr'] loss=1.747 err=0.348 valid=TIMIT_dev loss=1.913 err=0.360 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=311
ep=013 tr=['TIMIT_tr'] loss=1.706 err=0.341 valid=TIMIT_dev loss=1.909 err=0.356 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=314
ep=014 tr=['TIMIT_tr'] loss=1.670 err=0.335 valid=TIMIT_dev loss=1.897 err=0.355 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=307
ep=015 tr=['TIMIT_tr'] loss=1.634 err=0.329 valid=TIMIT_dev loss=1.897 err=0.355 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=307
ep=016 tr=['TIMIT_tr'] loss=1.596 err=0.323 valid=TIMIT_dev loss=1.945 err=0.364 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=309
ep=017 tr=['TIMIT_tr'] loss=1.497 err=0.306 valid=TIMIT_dev loss=1.862 err=0.347 lr_architecture1=0.000160 lr_architecture2=0.000160 lr_architecture3=0.000200 time(s)=314
ep=018 tr=['TIMIT_tr'] loss=1.461 err=0.300 valid=TIMIT_dev loss=1.849 err=0.346 lr_architecture1=0.000160 lr_architecture2=0.000160 lr_architecture3=0.000200 time(s)=294
ep=019 tr=['TIMIT_tr'] loss=1.439 err=0.297 valid=TIMIT_dev loss=1.836 err=0.341 lr_architecture1=0.000160 lr_architecture2=0.000160 lr_architecture3=0.000200 time(s)=312
ep=020 tr=['TIMIT_tr'] loss=1.417 err=0.293 valid=TIMIT_dev loss=1.847 err=0.342 lr_architecture1=0.000160 lr_architecture2=0.000160 lr_architecture3=0.000200 time(s)=303
ep=021 tr=['TIMIT_tr'] loss=1.368 err=0.285 valid=TIMIT_dev loss=1.816 err=0.336 lr_architecture1=0.000080 lr_architecture2=0.000080 lr_architecture3=0.000100 time(s)=312
ep=022 tr=['TIMIT_tr'] loss=1.345 err=0.281 valid=TIMIT_dev loss=1.827 err=0.338 lr_architecture1=0.000080 lr_architecture2=0.000080 lr_architecture3=0.000100 time(s)=299
ep=023 tr=['TIMIT_tr'] loss=1.317 err=0.277 valid=TIMIT_dev loss=1.811 err=0.334 lr_architecture1=0.000040 lr_architecture2=0.000040 lr_architecture3=0.000050 time(s)=368
%WER 15.9 | 192 7215 | 86.7 10.6 2.7 2.6 15.9 98.4 | -1.130 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_RNN_fmllr_lr000032_lran_monoref_v2/decode_TIMIT_test_out_dnn2/score_5/ctm_39phn.filt.sys

