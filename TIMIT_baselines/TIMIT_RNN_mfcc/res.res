ep=000 tr=['TIMIT_tr'] loss=6.474 err=0.773 valid=TIMIT_dev loss=3.538 err=0.605 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=332
ep=001 tr=['TIMIT_tr'] loss=3.636 err=0.611 valid=TIMIT_dev loss=2.930 err=0.522 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=305
ep=002 tr=['TIMIT_tr'] loss=3.124 err=0.548 valid=TIMIT_dev loss=2.619 err=0.480 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=324
ep=003 tr=['TIMIT_tr'] loss=2.873 err=0.514 valid=TIMIT_dev loss=2.492 err=0.458 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=324
ep=004 tr=['TIMIT_tr'] loss=2.699 err=0.490 valid=TIMIT_dev loss=2.421 err=0.449 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=329
ep=005 tr=['TIMIT_tr'] loss=2.564 err=0.472 valid=TIMIT_dev loss=2.338 err=0.434 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=328
ep=006 tr=['TIMIT_tr'] loss=2.460 err=0.455 valid=TIMIT_dev loss=2.282 err=0.425 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=329
ep=007 tr=['TIMIT_tr'] loss=2.370 err=0.442 valid=TIMIT_dev loss=2.239 err=0.420 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=325
ep=008 tr=['TIMIT_tr'] loss=2.291 err=0.430 valid=TIMIT_dev loss=2.167 err=0.410 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=318
ep=009 tr=['TIMIT_tr'] loss=2.212 err=0.418 valid=TIMIT_dev loss=2.181 err=0.409 lr_architecture1=0.000320 lr_architecture2=0.000320 lr_architecture3=0.000400 time(s)=320
ep=010 tr=['TIMIT_tr'] loss=2.063 err=0.395 valid=TIMIT_dev loss=2.074 err=0.395 lr_architecture1=0.000160 lr_architecture2=0.000160 lr_architecture3=0.000200 time(s)=327
ep=011 tr=['TIMIT_tr'] loss=2.017 err=0.388 valid=TIMIT_dev loss=2.053 err=0.390 lr_architecture1=0.000160 lr_architecture2=0.000160 lr_architecture3=0.000200 time(s)=335
ep=012 tr=['TIMIT_tr'] loss=1.977 err=0.382 valid=TIMIT_dev loss=2.052 err=0.390 lr_architecture1=0.000160 lr_architecture2=0.000160 lr_architecture3=0.000200 time(s)=322
ep=013 tr=['TIMIT_tr'] loss=1.903 err=0.371 valid=TIMIT_dev loss=2.023 err=0.384 lr_architecture1=0.000080 lr_architecture2=0.000080 lr_architecture3=0.000100 time(s)=327
ep=014 tr=['TIMIT_tr'] loss=1.883 err=0.368 valid=TIMIT_dev loss=2.003 err=0.381 lr_architecture1=0.000080 lr_architecture2=0.000080 lr_architecture3=0.000100 time(s)=322
ep=015 tr=['TIMIT_tr'] loss=1.859 err=0.365 valid=TIMIT_dev loss=1.994 err=0.380 lr_architecture1=0.000080 lr_architecture2=0.000080 lr_architecture3=0.000100 time(s)=323
ep=016 tr=['TIMIT_tr'] loss=1.849 err=0.362 valid=TIMIT_dev loss=1.990 err=0.379 lr_architecture1=0.000080 lr_architecture2=0.000080 lr_architecture3=0.000100 time(s)=329
ep=017 tr=['TIMIT_tr'] loss=1.827 err=0.359 valid=TIMIT_dev loss=1.997 err=0.380 lr_architecture1=0.000080 lr_architecture2=0.000080 lr_architecture3=0.000100 time(s)=320
ep=018 tr=['TIMIT_tr'] loss=1.800 err=0.356 valid=TIMIT_dev loss=1.986 err=0.380 lr_architecture1=0.000040 lr_architecture2=0.000040 lr_architecture3=0.000050 time(s)=323
ep=019 tr=['TIMIT_tr'] loss=1.780 err=0.352 valid=TIMIT_dev loss=1.970 err=0.376 lr_architecture1=0.000020 lr_architecture2=0.000020 lr_architecture3=0.000025 time(s)=324
ep=020 tr=['TIMIT_tr'] loss=1.767 err=0.351 valid=TIMIT_dev loss=1.976 err=0.378 lr_architecture1=0.000020 lr_architecture2=0.000020 lr_architecture3=0.000025 time(s)=328
ep=021 tr=['TIMIT_tr'] loss=1.761 err=0.349 valid=TIMIT_dev loss=1.971 err=0.377 lr_architecture1=0.000010 lr_architecture2=0.000010 lr_architecture3=0.000013 time(s)=319
ep=022 tr=['TIMIT_tr'] loss=1.761 err=0.348 valid=TIMIT_dev loss=1.970 err=0.377 lr_architecture1=0.000010 lr_architecture2=0.000010 lr_architecture3=0.000013 time(s)=325
ep=023 tr=['TIMIT_tr'] loss=1.755 err=0.348 valid=TIMIT_dev loss=1.966 err=0.376 lr_architecture1=0.000005 lr_architecture2=0.000005 lr_architecture3=0.000006 time(s)=326
%WER 17.8 | 192 7215 | 84.5 12.1 3.4 2.2 17.8 99.0 | -0.812 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_RNN_mfcc_lr000032_lran_monoref_v2/decode_TIMIT_test_out_dnn2/score_5/ctm_39phn.filt.sys

