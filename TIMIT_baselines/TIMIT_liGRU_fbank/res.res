ep=000 tr=['TIMIT_tr'] loss=5.155 err=0.724 valid=TIMIT_dev loss=3.101 err=0.544 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=594
ep=001 tr=['TIMIT_tr'] loss=3.017 err=0.524 valid=TIMIT_dev loss=2.400 err=0.445 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=583
ep=002 tr=['TIMIT_tr'] loss=2.420 err=0.442 valid=TIMIT_dev loss=2.160 err=0.410 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=619
ep=003 tr=['TIMIT_tr'] loss=2.167 err=0.404 valid=TIMIT_dev loss=2.028 err=0.394 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=622
ep=004 tr=['TIMIT_tr'] loss=1.982 err=0.376 valid=TIMIT_dev loss=1.977 err=0.387 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=625
ep=005 tr=['TIMIT_tr'] loss=1.847 err=0.357 valid=TIMIT_dev loss=1.871 err=0.368 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=619
ep=006 tr=['TIMIT_tr'] loss=1.729 err=0.338 valid=TIMIT_dev loss=1.862 err=0.367 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=619
ep=007 tr=['TIMIT_tr'] loss=1.634 err=0.323 valid=TIMIT_dev loss=1.793 err=0.356 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=625
ep=008 tr=['TIMIT_tr'] loss=1.549 err=0.311 valid=TIMIT_dev loss=1.774 err=0.355 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=630
ep=009 tr=['TIMIT_tr'] loss=1.480 err=0.299 valid=TIMIT_dev loss=1.740 err=0.347 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=623
ep=010 tr=['TIMIT_tr'] loss=1.412 err=0.288 valid=TIMIT_dev loss=1.746 err=0.345 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=620
ep=011 tr=['TIMIT_tr'] loss=1.360 err=0.281 valid=TIMIT_dev loss=1.741 err=0.346 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=630
ep=012 tr=['TIMIT_tr'] loss=1.213 err=0.256 valid=TIMIT_dev loss=1.680 err=0.332 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=616
ep=013 tr=['TIMIT_tr'] loss=1.162 err=0.247 valid=TIMIT_dev loss=1.692 err=0.333 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=622
ep=014 tr=['TIMIT_tr'] loss=1.094 err=0.236 valid=TIMIT_dev loss=1.667 err=0.327 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=629
ep=015 tr=['TIMIT_tr'] loss=1.066 err=0.231 valid=TIMIT_dev loss=1.660 err=0.325 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=625
ep=016 tr=['TIMIT_tr'] loss=1.047 err=0.227 valid=TIMIT_dev loss=1.668 err=0.324 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=622
ep=017 tr=['TIMIT_tr'] loss=1.028 err=0.224 valid=TIMIT_dev loss=1.669 err=0.324 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=626
ep=018 tr=['TIMIT_tr'] loss=1.014 err=0.222 valid=TIMIT_dev loss=1.673 err=0.323 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=633
ep=019 tr=['TIMIT_tr'] loss=0.993 err=0.218 valid=TIMIT_dev loss=1.673 err=0.322 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=624
ep=020 tr=['TIMIT_tr'] loss=0.987 err=0.217 valid=TIMIT_dev loss=1.684 err=0.323 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=609
ep=021 tr=['TIMIT_tr'] loss=0.952 err=0.211 valid=TIMIT_dev loss=1.681 err=0.321 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=630
ep=022 tr=['TIMIT_tr'] loss=0.940 err=0.209 valid=TIMIT_dev loss=1.677 err=0.320 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=621
ep=023 tr=['TIMIT_tr'] loss=0.933 err=0.208 valid=TIMIT_dev loss=1.690 err=0.321 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=629
%WER 14.5 | 192 7215 | 88.1 9.3 2.7 2.6 14.5 98.4 | -3.317 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_liGRU_fbank_5lay_lr00004_lran_monoreg/decode_TIMIT_test_out_dnn2/score_1/ctm_39phn.filt.sys

