ep=000 tr=['TIMIT_tr'] loss=5.068 err=0.703 valid=TIMIT_dev loss=2.764 err=0.510 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=599
ep=001 tr=['TIMIT_tr'] loss=2.642 err=0.478 valid=TIMIT_dev loss=2.169 err=0.410 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=580
ep=002 tr=['TIMIT_tr'] loss=2.123 err=0.400 valid=TIMIT_dev loss=1.989 err=0.380 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=626
ep=003 tr=['TIMIT_tr'] loss=1.909 err=0.367 valid=TIMIT_dev loss=1.962 err=0.374 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=630
ep=004 tr=['TIMIT_tr'] loss=1.749 err=0.342 valid=TIMIT_dev loss=1.863 err=0.357 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=635
ep=005 tr=['TIMIT_tr'] loss=1.633 err=0.324 valid=TIMIT_dev loss=1.818 err=0.352 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=625
ep=006 tr=['TIMIT_tr'] loss=1.529 err=0.307 valid=TIMIT_dev loss=1.794 err=0.347 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=650
ep=007 tr=['TIMIT_tr'] loss=1.445 err=0.293 valid=TIMIT_dev loss=1.797 err=0.344 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=638
ep=008 tr=['TIMIT_tr'] loss=1.366 err=0.281 valid=TIMIT_dev loss=1.747 err=0.338 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=623
ep=009 tr=['TIMIT_tr'] loss=1.304 err=0.271 valid=TIMIT_dev loss=1.737 err=0.334 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=630
ep=010 tr=['TIMIT_tr'] loss=1.241 err=0.261 valid=TIMIT_dev loss=1.769 err=0.335 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=622
ep=011 tr=['TIMIT_tr'] loss=1.110 err=0.238 valid=TIMIT_dev loss=1.716 err=0.325 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=632
ep=012 tr=['TIMIT_tr'] loss=1.061 err=0.229 valid=TIMIT_dev loss=1.737 err=0.323 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=635
ep=013 tr=['TIMIT_tr'] loss=1.025 err=0.224 valid=TIMIT_dev loss=1.737 err=0.321 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=655
ep=014 tr=['TIMIT_tr'] loss=0.996 err=0.219 valid=TIMIT_dev loss=1.726 err=0.320 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=635
ep=015 tr=['TIMIT_tr'] loss=0.967 err=0.214 valid=TIMIT_dev loss=1.740 err=0.320 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=617
ep=016 tr=['TIMIT_tr'] loss=0.902 err=0.202 valid=TIMIT_dev loss=1.759 err=0.317 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=625
ep=017 tr=['TIMIT_tr'] loss=0.883 err=0.198 valid=TIMIT_dev loss=1.763 err=0.316 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=629
ep=018 tr=['TIMIT_tr'] loss=0.865 err=0.195 valid=TIMIT_dev loss=1.761 err=0.315 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=632
ep=019 tr=['TIMIT_tr'] loss=0.850 err=0.192 valid=TIMIT_dev loss=1.780 err=0.317 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=627
ep=020 tr=['TIMIT_tr'] loss=0.821 err=0.187 valid=TIMIT_dev loss=1.781 err=0.316 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=655
ep=021 tr=['TIMIT_tr'] loss=0.810 err=0.185 valid=TIMIT_dev loss=1.790 err=0.315 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=633
ep=022 tr=['TIMIT_tr'] loss=0.806 err=0.184 valid=TIMIT_dev loss=1.791 err=0.315 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=616
ep=023 tr=['TIMIT_tr'] loss=0.790 err=0.181 valid=TIMIT_dev loss=1.790 err=0.313 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=630
%WER 14.1 | 192 7215 | 88.0 9.0 3.0 2.1 14.1 99.5 | -1.802 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_liGRU_fmllr_5lay_lr00004_lran_monoreg_v4/decode_TIMIT_test_out_dnn2/score_4/ctm_39phn.filt.sys

