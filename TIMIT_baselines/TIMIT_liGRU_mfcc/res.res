ep=000 tr=['TIMIT_tr'] loss=5.388 err=0.729 valid=TIMIT_dev loss=3.016 err=0.535 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=601
ep=001 tr=['TIMIT_tr'] loss=2.966 err=0.518 valid=TIMIT_dev loss=2.420 err=0.449 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=588
ep=002 tr=['TIMIT_tr'] loss=2.407 err=0.440 valid=TIMIT_dev loss=2.147 err=0.411 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=622
ep=003 tr=['TIMIT_tr'] loss=2.154 err=0.404 valid=TIMIT_dev loss=2.041 err=0.394 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=634
ep=004 tr=['TIMIT_tr'] loss=1.974 err=0.376 valid=TIMIT_dev loss=2.028 err=0.389 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=640
ep=005 tr=['TIMIT_tr'] loss=1.836 err=0.356 valid=TIMIT_dev loss=1.930 err=0.375 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=631
ep=006 tr=['TIMIT_tr'] loss=1.726 err=0.338 valid=TIMIT_dev loss=1.947 err=0.376 lr_architecture1=0.000400 lr_architecture2=0.000400 lr_architecture3=0.000400 time(s)=644
ep=007 tr=['TIMIT_tr'] loss=1.533 err=0.308 valid=TIMIT_dev loss=1.818 err=0.355 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=610
ep=008 tr=['TIMIT_tr'] loss=1.459 err=0.295 valid=TIMIT_dev loss=1.793 err=0.354 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=633
ep=009 tr=['TIMIT_tr'] loss=1.406 err=0.286 valid=TIMIT_dev loss=1.792 err=0.348 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=633
ep=010 tr=['TIMIT_tr'] loss=1.360 err=0.279 valid=TIMIT_dev loss=1.772 err=0.347 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=626
ep=011 tr=['TIMIT_tr'] loss=1.319 err=0.273 valid=TIMIT_dev loss=1.786 err=0.345 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=635
ep=012 tr=['TIMIT_tr'] loss=1.278 err=0.267 valid=TIMIT_dev loss=1.778 err=0.346 lr_architecture1=0.000200 lr_architecture2=0.000200 lr_architecture3=0.000200 time(s)=633
ep=013 tr=['TIMIT_tr'] loss=1.200 err=0.253 valid=TIMIT_dev loss=1.764 err=0.340 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=643
ep=014 tr=['TIMIT_tr'] loss=1.174 err=0.250 valid=TIMIT_dev loss=1.763 err=0.338 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=634
ep=015 tr=['TIMIT_tr'] loss=1.147 err=0.244 valid=TIMIT_dev loss=1.757 err=0.337 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=627
ep=016 tr=['TIMIT_tr'] loss=1.129 err=0.242 valid=TIMIT_dev loss=1.767 err=0.337 lr_architecture1=0.000100 lr_architecture2=0.000100 lr_architecture3=0.000100 time(s)=626
ep=017 tr=['TIMIT_tr'] loss=1.089 err=0.234 valid=TIMIT_dev loss=1.760 err=0.335 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=624
ep=018 tr=['TIMIT_tr'] loss=1.076 err=0.233 valid=TIMIT_dev loss=1.774 err=0.336 lr_architecture1=0.000050 lr_architecture2=0.000050 lr_architecture3=0.000050 time(s)=629
ep=019 tr=['TIMIT_tr'] loss=1.055 err=0.229 valid=TIMIT_dev loss=1.768 err=0.334 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=628
ep=020 tr=['TIMIT_tr'] loss=1.051 err=0.228 valid=TIMIT_dev loss=1.774 err=0.334 lr_architecture1=0.000025 lr_architecture2=0.000025 lr_architecture3=0.000025 time(s)=628
ep=021 tr=['TIMIT_tr'] loss=1.039 err=0.226 valid=TIMIT_dev loss=1.771 err=0.334 lr_architecture1=0.000013 lr_architecture2=0.000013 lr_architecture3=0.000013 time(s)=629
ep=022 tr=['TIMIT_tr'] loss=1.034 err=0.225 valid=TIMIT_dev loss=1.771 err=0.333 lr_architecture1=0.000006 lr_architecture2=0.000006 lr_architecture3=0.000006 time(s)=642
ep=023 tr=['TIMIT_tr'] loss=1.029 err=0.224 valid=TIMIT_dev loss=1.772 err=0.334 lr_architecture1=0.000006 lr_architecture2=0.000006 lr_architecture3=0.000006 time(s)=623
%WER 15.3 | 192 7215 | 86.5 10.1 3.4 1.8 15.3 97.9 | -1.222 | /scratch/ravanelm/pytorch-kaldi-new/exp/TIMIT_liGRU_mfcc_5lay_lr00004_lran_monoreg_v2/decode_TIMIT_test_out_dnn2/score_5/ctm_39phn.filt.sys

